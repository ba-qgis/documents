#!/usr/bin/env bash

set -x

pdflatex -interaction=nonstopmode doku 
biber doku 
makeindex -s doku.ist -t doku.glg -o doku.gls doku.glo 
pdflatex -interaction=nonstopmode doku 
pdflatex -interaction=nonstopmode doku
