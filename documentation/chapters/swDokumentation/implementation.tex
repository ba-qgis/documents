\chapter{Implementation}
\label{chap:implementation}
\section{\gls{QGIS} Erweiterung}
Wenn man eine Erweiterung für \gls{QGIS} schreibt, dann gibt es zwei direkte und eine indirekte Möglichkeit. Die beiden direkten sind ein Plugin oder Processing Algorithmen zu erstellen. Die indirekte ist ein Backendserver, der die Berechnung durchführt. Alle diese Möglichkeiten haben verschiedene Vor- und Nachteile, die in diesem Abschnitt genauer erläutert werden.

\subsection{Plugin}
Eine der direkten Möglichkeiten ist es, dass man als Erweiterung ein Plugin macht. Das Plugin lässt sich sehr einfach über den \gls{QGIS} Plugin Manager installieren und verbreiten. Beim Erstellen eines Plugins kann das GUI sehr einfach spezifiziert werden. Dem Entwickler ist dabei die Freiheit überlassen.

Jedoch ist das Ganze eine Blackbox, wo alles einfach fix an einem Stück durchläuft. Die Plugins stehen immer alleine und können nicht von anderen Plugins abhängig sein.

\subsection{Processing Algorithmus}
Processing Algorithmen sind eine andere Art einer direkten \gls{QGIS} Erweiterung. Ein Processing Algorithmus ist dafür da, genau eine Funktionalität zu haben oder eine Berechnung durchzuführen. Es ist ausserdem möglich, mehrere dieser Processing Algorithmen mithilfe des Modellbaukastens aneinanderzureihen, damit diese am Stück ausgeführt werden. Da alles von einander getrennt implementiert wird, ist es auch sehr einfach, alle Schritte separat zu den anderen laufen zu lassen und auch gewisse Teile auszutauschen.

Installieren kann man das Ganze zwar über den Plugin Store, jedoch muss dafür ein Plugin geschrieben werden, das die Installation übernimmt.

\subsection{Backendserver}
Die Implementation könnte man auch indirekt machen, indem man alle Berechnungen auf einem Backendserver ausführt. Dies würde alle Abhängigkeiten von \gls{QGIS} entfernen, da \gls{QGIS} bei dieser Möglichkeit nur als Frontend gilt. So wäre es auch möglich, noch weitere Frontends an diesen Server anzuschliessen. 

Da die Berechnung sehr aufwendig ist und ein Backend von mehreren Benutzern gleichzeitig benutzt werden kann, muss es sich um einen leistungsstarker Server handeln, was sehr teuer wird. Ausserdem muss dieser Server auch noch deployed werden, was zu weiteren Problemen und Risiken führen kann.

\subsection{Entscheidung}
Der Backendserver macht bei dieser Arbeit keinen Sinn, da nur Kosten und Risiken dazu kommen. Es ist auch nicht nötig, dass es weitere Frontends gibt für dieses Projekts. Es wäre nur nötig diese Möglichkeit zu nutzen, wenn es mit \gls{QGIS} unmöglich ist, den Algorithmus zu implementieren.

\yapproach%
{Plugin}%
{Processing Algorithmus}%
{Die Entscheidung ist auf die Processing Algorithmen gefallen, da es die Vorteile des Zusammenkettens und auch Auszutauschens gibt, was bei dieser Berechnung die Weiterentwicklung stark vereinfacht.}

\newpage

\section{Berechnung}
\begin{figure}[H]
	\centering
		\includegraphics[scale=0.6]{images/berechnung_Zersiedelung}
	\caption{Berechnung der Zersiedelung}
\end{figure}

Die Berechnung der Zersiedelung wurde in mehrere Processing Algorithmen verteilt. Dies hat den Vorteil, dass alles entweder an einem Stück oder einzeln durchgerechnet werden kann, je nach dem Bedürfnis des Benutzers.

\subsection{Input Daten}
Die Inputdaten, wenn man alles an einem Stück durchrechnet, sind so einfach wie möglich gehalten, so dass es jeder auch ohne grosse Vorkenntnisse bedienen kann. Die Inputdaten sind:
\begin{itemize}
	\item Raster mit dem bebauten Gebiet
	\item Ein Vektor Polygon mit den Gebietsgrenzen
	\item Anzahl der Bewohner in dem Gebiet
	\item Anzahl der Angestellten in dem Gebiet
	\item Der \gls{ASF} Wert für die Gewichtung
\end{itemize}
Die Anzahl der Bewohner oder der Angestellten kann auch zusammen eingetragen werden. Dies erreicht der Benutzer einfach, indem er einen der Werten auf 0 lässt.

Der Radius ist konstant auf 2'000 Meter gesetzt, da je nach Radius die Gewichtungsfunktionen anders sind. Mehr dazu in \autoref{sec:gewichtung}.


\subsection{Clip Raster Layer}
Der Clip Raster Layer Processing Algorithmus ist der Erste, der aufgerufen werden sollte. Dieser stellt sicher, dass nur die Fläche in den Gebietsgrenzen genutzt wird. Dies wird sichergestellt, indem wir die Daten des Rasters normalisieren. 

Für das Ausschneiden des Rasters aus dem Polygon wurde von GDAL \cite{gdal} ein Processing Algorithmus verwendet. Der Algorithmus von GDAL mit dem Namen 'Clip raster by Mask' braucht als Input ein Vektor Polygon und ein Raster und gibt als Output das ausgeschnittene Raster zurück. Dieses Raster wird dann in eine Numpy \cite{numpy} Matrix übertragen und schlussendlich normalisiert. Diese Matrix wird danach wieder in ein Raster umgewandelt, welches der Output des Algorithmus ist.

\subsection{\gls{SI} Berechnung}
Die Berechnung von \gls{SI} \cite{schwickjaeger09} ist eine der wichtigsten Berechnungen, da diese am längsten dauert. Bei der Berechnung von \gls{SI} handelt es sich darum, dass ein \gls{Pixel} im Raster genommen wird, der als bebaut markiert wurde. Von diesem \gls{Pixel} aus wird die Distanz zu allen anderen \gls{Pixel}, die als bebaut markiert und in einem bestimmten Radius sind, aufsummiert. Davon wird der Durchschnitt berechnet. Dies macht man für jeden bebauten \gls{Pixel} im Raster.

Es werden zwei Raster gebraucht. Eines welches vom ersten Processing Algorithmus (Clip Raster Layer) erzeugt wird (Clipped Raster) und ein zweites, welches über die Gebietsgrenzen hinaus geht. Das zweite Raster wird benötigt, damit die Berechnung der Dispersion später genauer ist.

Die beiden Raster werden in Numpy Matrizen umgewandelt. Die Clipped Raster Matrix wird mit einem Loop durchlaufen. Wenn ein \gls{Pixel} als bebaut markiert ist, wird die Position dieses \gls{Pixel}s gespeichert und im zweiten Raster gesucht. Diese Koordinaten sind der Mittelpunkt für die Berechnung. Das zweite Raster wird in zwei Loops durchlaufen. Der erste Loop in die X-Richtung und der zweite in die Y-Richtung. Die Grenzen der Loops wird der Radius geteilt durch die \gls{Pixel}grösse gesetzt. Dadurch weiss man wie viele \gls{Pixel} überprüft werden müssen in der Berechnung. Dies hat den Vorteil, dass nicht bei jedem Loop überprüft werden muss, ob der \gls{Pixel} sich im Radius befindet.

Für die \gls{Pixel}, die sich im Radius befinden und als bebaut markiert sind, wird die euklidische Distanz \cite{distance} zum Ursprungspixel aus berechnet.

$$ \sqrt{\sum_{i=1}^{2}(y_i - x_i)^2}$$

Die Distanzen werden aufsummiert und die Anzahl gezählt. Davon wird der Durchschnitt berechnet und am Ende wird dieser Wert in einer neuen Matrix, an dieselbe Position, wie der Ursprungspixel geschrieben.

Aus dieser neuen Matrix wird dann ein Raster erzeugt mit den neuen Werten. Dieses Raster kann man einfärben, um die Dispersion noch farblich darzustellen.

\subsection{\gls{DIS} Berechnung}
Als Input für die \gls{DIS} Berechnung wird das vorher berechnete \gls{SI} Raster benötigt. Der \gls{DIS} Wert sagt aus, wie gross die Dispersion im ganzen Gebiet ist. Dafür wird das \gls{SI} Raster in eine Matrix umgewandelt und alle \gls{Pixel}, die einen \gls{SI} Wert gehabt haben, werden zusammen gezählt und der Durchschnitt daraus berechnet.

\subsection{\gls{FI} Berechnung}
Für die Berechnung des \gls{FI} Wertes wird sowohl die ausgeschnittene Siedlungsfläche als auch die Anzahl der Einwohner und Angestellten in der Gebietsgrenze gebraucht. Die beiden Anzahlen werden zusammen gezählt und durch die Siedlungsfläche geteilt.

\subsection{Gewichtung}
\label{sec:gewichtung}
Für die Berechnung der Grösse \gls{Z} müssen der \gls{DIS} Wert und der \gls{FI} Wert gewichtet werden. Wichtig bei der Gewichtung ist zu beachten, dass diese Formeln nur gelten, wenn der Radius auf 2'000 Meter gesetzt wird. Ansonsten müssen die Formeln auf den ausgewählten Radius angepasst werden. Die Formeln sind definiert nach Schwick und Jäger \cite{schwickjaeger}.

\subsubsection{\gls{DIS} Gewichtung}
Die Gewichtung der Dispersion ist ein Wert, der sich zwischen 0.5 und 1.5 befindet. Das Ziel dieser Gewichtung ist es, dass höhere Gebiete mit einer höheren Dispersion einen grösseren Einfluss haben und weniger disperse einen kleineren.

Die Formel für die Gewichtung:
$$ 0.5 + \frac{e^{0.294432 * DIS - 12.955}}{1 + e^{0.294432 * DIS - 12.955}} $$

\subsubsection{FI Gewichtung}
Die Gewichtung des \gls{FI} Wertes befindet sich zwischen 0 und 1. Wenn die Fläche besser genutzt wird, sollte diese einen kleineren Einfluss haben auf die Grösse \gls{Z}, als wenn diese schlecht genutzt wird.

Die Formel für die Gewichtung:
$$ \frac{e^{4.159 - 613.125 / FI}}{1 + e^{4.159 - 613.125 / FI}} $$

\subsection{Z Berechnung}
Die Grösse \gls{Z} ist die Zersiedelung und somit das Endresultat dieses Algorithmus. Als Input wird \gls{DIS}, \gls{ASF} und \gls{FI} benötigt. Die Formel setzt sich dann wiefolgt zusammen:

$$ Z = ASF * DIS * g_1(DIS) * g_2(FI)$$

Wobei $g_1$ die \gls{DIS} Gewichtungsfunktion ist und $g_2$ die \gls{FI} Gewichtungsfunktion.

\subsection{Probleme}
\subsubsection{Parallelisierung}
\label{sec:parallelisierung}

\gls{QGIS} bietet eine eigene Implementation von QT \cite{qt} QThreadPool an, welche QgsTaskManager \cite{qgisdoku} heisst. Um diesen Taskmanager zu verwenden, muss man zuerst einen QgsTask \cite{qgisdoku} implementieren, welcher entweder eine Methode oder Klasse sein kann. Die Implementierung dieser Task wird auf OpenGIS \cite{qgstask} beschrieben.

Da die Berechnung von \gls{SI} sehr lange dauert und gut parallelisiert werden kann, macht es am meisten Sinn diese Berechnung zu parallelisieren. Die kleinste mögliche Einheit, in die die \gls{SI} Kalkulation unterteilt werden kann, ist ein einzelner bebauter \gls{Pixel} und die Distanz-Berechnung zwischen diesem \gls{Pixel} und allen \gls{Pixel} in einem bestimmten Radius. Aus diesem Grund wird für jeden bebauten \gls{Pixel} des Rasters einen Task erstellt, welche dann an den Taskmanager übergeben werden. Wenn alle Tasks dem Taskmanager übergeben wurden, muss nur noch gewartet werden, bis alle Tasks beendet sind. Danach kann man aus den Resultaten ein neues Raster erzeugen.

Hier tauchen die ersten Probleme auf. Je mehr Tasks man gleichzeitig dem Taskmanager übergibt, desto wahrscheinlicher wird es, dass nicht alle Tasks gestartet und dann auch beendet werden. Um dieses Problem zu lösen, wurde ein Loop implementiert, der nur eine bestimmte Anzahl von Tasks dem Taskmanager übergibt und wartet, bis diese vollendet sind. Leider stellt dies nur sicher, dass alle Tasks starten, aber nicht dass sie auch alle beendet werden. Um zu überprüfen, ob ein Task fertig ist, gibt es eine \textbf{isActive()} Methode auf dem QgsTask. Das Problem ist aber, dass in manchen Fällen immer \textbf{True} retourniert wird, auch wenn der Task fertig ist.

QgsTask haben eine \textbf{cancel()} Methode um den Task abzubrechen. Beim Aufruf dieser Methode wird ein Signal an den Task gesendet, was dazu führt, dass die Methode \textbf{isCancled()} \textbf{True} retourniert. Dies funktioniert aber nicht sehr zuverlässig und hat die Konsequenz, dass Tasks existieren, die nicht beendet werden können, auch wenn die Methode mehrmals aufgerufen wird.

QgsTask hat noch eine weiter Methode \textbf{status()}. Diese Methode hat das Problem, dass in manchen Fällen der Status nie von \textbf{Running} auf \textbf{Completed} oder \textbf{Termineted} wechselt.

Ein weiteres Problem ist, dass für alle Tasks ein globaler QThreadPool verwendet wird. Welcher aber auf eine fixe Anzahl Threads limitiert ist, was dazu führt, dass man eine maximale Auslastung von 60-70\% auf Windows und auf Linux von 40\% erzielt. Dies kann auch nicht optimiert werden, wenn man die maximale Anzahl Threads des globalen QThreadPool erhöht oder in den \gls{QGIS} Einstellung die maximale Anzahl Threads anpasst.

Mehrere Tests haben auch ergeben, dass die Berechnung schneller ist ohne \gls{QGIS} Parallelisierung. Eine weitere Option um zu parallelisieren ist es Python Multiprocessing \cite{pythondoku} zu verwenden. Dies funktioniert aber mit \gls{QGIS} auf Windows nicht, da Windows Threads kein \textbf{fork} haben \cite{pythonmultiprocessing}.

Aus den oben genanten Gründen ist es momentan nicht möglich, \gls{QGIS} Komputation zuverlässig zu parallelisieren.
